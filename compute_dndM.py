#!/usr/bin/python2
import os
import sys
import h5py
import numpy as np

def function(M, lbox, nbin):
    M = np.array(M)
    logM = np.log10(M[M != 0.0])
    nbin = np.power(10., np.linspace(min(logM), max(logM), nbin)
    hist, m_edge = np.histogram(logM, nbin)
    deltaM = m_edge[1] - m_edge[0]
    mass_arr = r_edge[1:] - 0.5*deltaM
    dlnM = np.power(10., mass_arr[1:]) - np.power(10., mass_arr[0:len(nbin)])
    dn = hist * np.power(10., mass_arr/dlnM) * np.log10(2.71828)
    volume = np.float(np.power(lbox, 3.))
    return [np.power(10, mass_arr), np.power(10. marr_arr) * dn/volume]

if(len(sys.argv) != 4):
    print "*** Usage:", os.path.basename(__file__), "<mass array> <box length> <bin number>"
    sys.exit(1337)

print massfunction(sys.argv[1],sys.argv[2],sys.argv[3])
