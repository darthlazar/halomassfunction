#!/usr/bin/env python2

# system ----
from __future__ import absolute_import, division
import os
import sys
import warnings
import numpy as np
import scipy.integrate as integrate

class Cosmology(object):
    """
    A cosmology is set by the initial parameters pass to the class.
    A complete set of pre-determined cosmologies (flat - Lambda Cold Dark Matter)
    should be found in dicts/.

    By default, the cosmology used without any pass arguments will be the PLANCK15
    parameters. This includes the present day CMB temperature (`Tcmb0` = 2.7255) and
    the effective number of neutrino species (`Neff` = 3.046). These values are compartible
    with the most recent measurements.

     * Parameters
    /* -------------------------------
    z: float
        The redshift at which the all the parameters pass will be evaluated at.
    h0: float
        The hubble parameter.
    omegaM: float
        The matter density in units of the critical density at z=0. Including all
        non-relativistic matter (i.e. dark matter and baryons)
    omegab: float
        The baryon density in units of the critical density at z=0.
    omegaL: float
        The dark energy density in units of the critical density at z=0.
    n_s: float
        The tilt of the primordial power spectrum.
    sigma8: float
        The normalization of the power spectrum -- the variance when the field is filtered
        with a top hat filter of radius 8 Mpc/h.
    Tcmb0: float
        The temperature of the CMB at z = 0 in Kelivn.
    Neff: float
        The effective number of neutrino species.
    rel : bool
        The decision to include relativistic species or not. But default, it it True.
    """

    def __init__(self, z=0.0,
            h0=0.6774, omegaM=0.3089, omegab=0.0486, n_s=0.9667, sigma8=0.8159,
            Tcmb0 = 2.7255, Neff=3.046, rel=True,
            **kwargs):
        self.z      = z
        self.h0     = h0
        self.omegaM = omegaM
        self.omegab = omegab
        self.omegaL = 1. - self.omegaM - self.omegab
        self.n_s    = n_s
        self.sigma8 = sigma8
        self.Tcmb0  = Tcmb0
        self.Neff   = Neff
        self.rel    = rel

    def scale(self,z):
        return 1./(1+z)

    def H0(self):
        return self.h0 * 100. # km/s/Mpc

    def E(self,z):
        omegaMz = (self.omegaM + self.omegab) * np.power(1.+z,3.)
        omegaRadz = (self.omegaRad()) * np.power(1.+z,4.)
        return np.sqrt(omegaMz + omegaRadz + self.omegaL)

    def hubble(self, z):
        return self.H0() * self.E(z) # km/s/Mpc

    def omega(self, z):
        return self.omegaM * np.power(1.+z,3.)/np.power(self.E(z),2.)

    def omegaRad(self):
        result = 0
        if(self.rel == True):
            omegaGamma = 4.4813e-7/self.h0 * np.power(self.Tcmb0, 4.)
            omegaNu = 7./8. * np.power(4./11.,4./3.) * self.Neff * omegaGamma
            result += omegaGamma + omegaNu
        else:
            result += 0.
        return result

    def rho_c(self, z):
        H = self.hubble(z) # [km/s/Mpc]
        G = 6.67e-11 # [m^3/kg/s^2]
        result = 3*np.power(H,2.)/(8.*np.pi*G) # [km^2 * Mpc^-2 * m^-3 * kg]
        convert = np.power(3.24078e-20,2.) * 1e-3 # kg * km^2/Mpc^3 --> g * Mpc^2/
        return convert * result # [g/cm^3]

    def rho_m(self, z):
        return self.rho_c(z) * self.omega(z) # [g/cm^3]

class Growth(Cosmology):
    """
    The linear growth factor for a LCDM cosmology that is a one of the growing modes in
    the linear regime.
    """

    def __init__(self, **kwargs):
        super(Growth,self).__init__(**kwargs)

    def factor(self, z):
        integrand = lambda x: (1.+x)/np.power(self.E(x),3.)
        if type(z) is np.ndarray:
            results = np.array([0.0] * z.size, dtype='float')
            for i in range(0,z.size):
                results[i] = 5./2 * self.omegaM * self.E(z[i]) * integrate.quad(integrand, z[i], np.inf)[0]
        else:
            integrand = lambda x: (1.+x)/np.power(self.E(x),3.)
            results = 5./2 * self.omegaM * self.E(z) * integrate.quad(integrand, z, np.inf)[0]
        return results

    def d(self,z):
        return self.factor(z)/self.factor(0.0)



