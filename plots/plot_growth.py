#!/usr/bin/env python2

# system ----
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

# local ----
sys.path.append('..')
import plot_parameters
from cosmology import Growth

values = {
        'h0' : 1.,
        'omegaM' : 0.3111,
        'omegab' : 0.0490,
        'omegaL' : 0,
        'n_s'    : 0.9665,
        'sigma8' : 0.8102
        }

redshift = np.linspace(0,11,500)

fig = plt.figure()
ax = fig.add_subplot(111)

growth = Growth(**values).factor(redshift)
ax.plot(redshift,growth)

ax.set_yscale('log')
ax.set_xlabel(r'$ z $')
ax.set_ylabel(r'$ D^{+}(z) $')


fig = plt.figure()
ax = fig.add_subplot(111)

growth = Growth(**values).norm(redshift)
ax.plot(redshift,growth**2.)

ax.set_yscale('log')
ax.set_xlabel(r'$ z $')
ax.set_ylabel(r'$ D^{+}(z)/D^{+}(z=0) $')

plt.show()

