#!/usr/bin/env python2

# system ----
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

# local ----
sys.path.append('..')
import plot_parameters
import power
import fittings

M_arr = 10**np.linspace(np.log10(1e8), np.log10(1e+15), 100)
#sigma_arr = power.Linear().sigma(0.0, M_arr)

press = fittings.Press74().fsigma(0.0, M_arr)
sheth = fittings.Sheth99().fsigma(0.0, M_arr)
jenkins = fittings.Jenkins01().fsigma(0.0, M_arr)
reed03 = fittings.Reed03().fsigma(0.0, M_arr)
warren = fittings.Warren06().fsigma(0.0,M_arr)
reed07 = fittings.Reed07().fsigma(0.0,M_arr)
tinker = fittings.Tinker08().fsigma(0.0,M_arr)
crocce = fittings.Crocce10().fsigma(0.0,M_arr)
courtin = fittings.Courtin10().fsigma(0.0,M_arr)
bhatta = fittings.Bhattacharya11().fsigma(0.0,M_arr)
angulo = fittings.Angulo12().fsigma(0.0,M_arr)
angulo_sub = fittings.Angulo12_SUB().fsigma(0.0,M_arr)
watson_fof = fittings.Watson13_FOF().fsigma(0.0,M_arr)
watson_so = fittings.Watson13_SO().fsigma(0.0,M_arr)

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(M_arr, press,
        color='royalblue')
ax.plot(M_arr[sheth > 0.], sheth[sheth > 0.],
        color='goldenrod')
ax.plot(M_arr[jenkins > 0.], jenkins[jenkins > 0.],
        color='limegreen', ls='-')
ax.plot(M_arr[reed03 > 0.], reed03[reed03>0.],
        color='indianred', ls="-")
ax.plot(M_arr[warren > 0.], warren[warren > 0.],
        color='c', ls="-")
ax.plot(M_arr[reed07 > 0.], reed07[reed07 > 0.],
        color='darkorange', ls="-")
ax.plot(M_arr[tinker > 0.], tinker[tinker > 0.],
        color='orchid', ls="-")
ax.plot(M_arr[crocce > 0.], crocce[crocce > 0.],
        color='royalblue', ls="--")
ax.plot(M_arr[courtin > 0.], courtin[courtin > 0.],
        color='goldenrod', ls="--")
ax.plot(M_arr[bhatta > 0.], bhatta[bhatta > 0.],
        color='limegreen', ls='--')
ax.plot(M_arr[angulo > 0.], angulo[angulo>0.],
        color='indianred', ls="--")
ax.plot(M_arr[angulo_sub > 0.], angulo_sub[angulo_sub>0.],
        color='c', ls="--")
ax.plot(M_arr[watson_fof > 0.], watson_fof[watson_fof>0.],
        color='darkorange', ls="--")
ax.plot(M_arr[watson_so > 0.], watson_so[watson_so>0.],
        color='orchid', ls="--")


ax.set_xlabel(r'$M_{\rm halo}\ [h^{-1}\ M_{\odot} ]$')
ax.set_ylabel(r'$f(\sigma)$')
ax.set_xscale('log')
ax.set_xlim(1e+8, 1e+15)
ax.set_ylim(0.0,0.5)

#fig.savefig('fits.png', format='png')

plt.show()


