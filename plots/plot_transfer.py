#!/usr/bin/env python2

# system ----
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

# local ----
sys.path.append('..')
import plot_parameters
import transfer

values = {
        'h0' : 1.,
        'omegaM' : 0.3089,
        'omegab' : 0.0486,
        'omegaL' : 1.000 - (0.3089+0.0486) ,
        'n_s'    : 0.9667,
        'sigma8' : 0.8159
        }

k_arr = np.linspace(1.e-2, 1e+1, 1000)

# Extraction via Transfer instances
bardeen86_adia = transfer.Transfer(perturbations='adiabatic', **values).bardeen86.function(k_arr)
bardeen86_iso = transfer.Transfer(perturbations='isocurvature', **values).bardeen86.function(k_arr)
bode01_005 = transfer.Transfer(m_x=0.05,**values).bode01.function(k_arr)
bode01_010 = transfer.Transfer(m_x=0.1,**values).bode01.function(k_arr)
bode01_020 = transfer.Transfer(m_x=0.2,**values).bode01.function(k_arr)
eisen98_zb = transfer.Transfer(**values).eisenstein98_zb.function(k_arr)
eisen98 = transfer.Transfer(h0=0.5, omegaM=0.25, omegab=0.75).eisenstein98.function(k_arr)

'''
# Extraction via Classes
bardeen86_adia = transfer.Bardeen86(perturabations='adiabatic',**values).function(k_arr)
bardeen86_iso = transfer.Bardeen86(perturbations='isocurvature',**values).function(k_arr)
eisen98 =  transfer.Eisenstein98(**values).function(k_arr)
eisen98_zb = transfer.Eisenstein98_zb(**values).function(k_arr)
bode01_005 = transfer.Bode01(m_x=0.5,**values).function(k_arr)
bode01_010 = transfer.Bode01(m_x=0.5,**values).function(k_arr)
bode01_020 = transfer.Bode01(m_x=0.5,**values).function(k_arr)
'''

# plot
fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(k_arr, abs(bardeen86_adia), color='black')
ax.plot(k_arr, abs(eisen98_zb), color='black', ls="--")
ax.plot(k_arr, abs(eisen98), color='black', ls=":")
ax.plot(k_arr, abs(bardeen86_iso), color='steelblue')
ax.plot(k_arr, abs(bode01_005), color='indianred')
ax.plot(k_arr, abs(bode01_010), color='indianred', ls='--')
ax.plot(k_arr, abs(bode01_020), color='indianred', ls='-.')

ax.set_yscale('log')
ax.set_xscale('log')
ax.set_xlim(1e-2,1e+1)
ax.set_ylim(1e-4,1e+1)

ax.set_xlabel(r'$k\ [h\ \rm Mpc^{-1}]$')
ax.set_ylabel(r'$|T(k)|$')

fig.savefig('transfer.png', format='png')

plt.show()

