#!/usr/bin/env python2

# system ----
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import timeit

# local ----
sys.path.append('..')
import plot_parameters
from power import Linear


k_arr = 10**np.linspace(np.log10(1.e-4), np.log10(10), 100)

start = timeit.default_timer()
bard_adia = Linear(transfer_model='bardeen86', perturbations='adiabatic').spectrum(0,k_arr)
bard_iso = Linear(transfer_model='bardeen86', perturbations='isocurvature').spectrum(0,k_arr)
bard_wdm005 = Linear(wdm_effects=True, m_x=0.05).spectrum(0,k_arr)
bard_wdm010 = Linear(wdm_effects=True, m_x=0.1).spectrum(0,k_arr)
bard_wdm020 = Linear(wdm_effects=True, m_x=.2).spectrum(0,k_arr)
eis_zb = Linear(omegab = 0, transfer_model='eisenstein98_zb').spectrum(0,k_arr)
eis = Linear(transfer_model='eisenstein98').spectrum(0,k_arr)
stop = timeit.default_timer()

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(k_arr, eis, color='black')
ax.plot(k_arr, eis_zb, color='black', ls='--')
ax.plot(k_arr, bard_adia, color='black', ls=':')
ax.plot(k_arr, bard_iso, color='steelblue')
ax.plot(k_arr, bard_wdm005, color='indianred')
ax.plot(k_arr, bard_wdm010, color='indianred', ls='--')
ax.plot(k_arr, bard_wdm020, color='indianred', ls='-.')

ax.set_xscale('log')
ax.set_yscale('log')

ax.set_xlabel(r'$k\ [h\ \rm Mpc^{-1}]$')
ax.set_ylabel(r'$P(k)\ [h^{-3} \rm Mpc^{3}]$')

ax.set_xlim(1e-4, 1e+1)
ax.set_ylim(1e0,1e5)

plt.show()
