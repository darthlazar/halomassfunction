#!/usr/bin/env python2

# system ----
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

# local ----
sys.path.append('..')
import plot_parameters
import power

M_arr = 10**np.linspace(np.log10(1e8), np.log10(1e+15), 100)


fig = plt.figure()
ax = fig.add_subplot(111)
sigma = power.Linear().sigma(0.0, M_arr)
ax.plot(M_arr, sigma)
ax.set_xscale('log')

fig = plt.figure()
ax = fig.add_subplot(111)
inv_sigma = -np.log(power.Linear().sigma(0.0, M_arr))
ax.plot(M_arr, inv_sigma)
ax.set_xscale('log')

plt.show()

