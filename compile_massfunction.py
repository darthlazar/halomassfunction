#!/usr/bin/env python2

# system ----
from __future__ import absolute_import, division
import os
import sys
import warnings
import numpy as np
import ConfigParser

# local ----
import dictionaries
import halomassfunction

if(len(sys.argv) != 2):
    print "Usage:", os.path.basename(__file__), "<*.cfg>"
    print "Exiting..."
    sys.exit(1337)

config = ConfigParser.ConfigParser()
config.read(sys.argv[1])

param_dict = {}
param_dict['cosmology'] = config.get('CDM', 'COSMOLOGY')
param_dict['transfer'] = config.get('CDM', 'TRANSFER_MODEL')
param_dict['filter'] = config.get('HMF', 'FILTER_SHAPE')
param_dict['fit'] = config.get('HMF', 'FITTING_FUNCTION')

parameters = {}
for dict_name, param_name in param_dict.items():
    try:
        dictionaries.all_dict[dict_name+'_dict'][param_name]
    except KeyError:
        print str(param_name), "is not a saved parameter.. refer to the readme for list!"
        sys.exit(1337)
    else:
        pass

parameters = {}
parameters.update(dictionaries.cosmology_dict[param_dict['cosmology']])
parameters['transfer_model'] = dictionaries.transfer_dict[param_dict['transfer']]
parameters['filter_shape'] = dictionaries.filter_dict[param_dict['filter']]
parameters['fit_func'] = dictionaries.fit_dict[param_dict['fit']]

if(config.has_section('WDM')):
    parameters['wdm_effects'] = config.get('WDM', 'WDM_EFFECTS')
    parameters['wdm_mass'] = config.getfloat('WDM', 'WDM_MASS')
else:
    pass

redshift = config.getfloat('HMF', 'REDSHIFT')
Mmin = config.getfloat('HMF', 'MASS_MIN')
Mmax = config.getfloat('HMF', 'MASS_MAX')
function = config.get('HMF', 'FUNCTION')

mass_array = 10.**np.linspace(np.log10(Mmin),np.log10(Mmax),100)

print halomassfunction.Function(**parameters).__getattribute__(function)(redshift, mass_array)

