#!/usr/bin/env python2

# system ----
from __future__ import absolute_import, division
import os
import sys
import warnings
import numpy as np

# local ----
from cosmology import Growth

class Transfer(Growth):

    def __init__(self, m_x=0.5, g_x=1.5, nu=1.2, perturbations='adiabatic', **kwargs):
        super(Transfer,self).__init__(**kwargs)
        self.bardeen86 = Bardeen86(perturbations, **kwargs)
        self.eisenstein98 = Eisenstein98(**kwargs)
        self.eisenstein98_zb = Eisenstein98_zb(**kwargs)
        self.bode01 = Bode01(m_x, g_x, nu, **kwargs)

class Bardeen86(Growth):

    def __init__(self, perturbations='adiabatic', **kwargs):
        super(Bardeen86, self).__init__(**kwargs)
        self.perturbations = perturbations

    def parameterShape(self):
        gamma = self.omegaM * self.h0
        if(self.omegab >= self.omegaM):
            print "The baryon denisty is too high for this numerical fit, leaving the results inaccurate. Please use the Eisenestin fit instead to accomodate the BAOs."
            sys.exit(1337)
        else:
            gamma *= np.exp(-self.omegab * (1. + np.sqrt(2.*self.h0)/self.omegaM))
        return gamma

    def adiabatic(self, k):
        kh = k * self.h0
        q = k/self.parameterShape()
        A = np.log(1. + 2.34*q)/(2.34*q)
        B = 1. + 3.89*q + np.power(16.1*q,2.) + np.power(5.46*q,3.) + np.power(6.71*q,4.)
        return A * np.power(B,-0.25)

    def isocurvature(self, k):
        kh = k * self.h0
        q = k/self.parameterShape()
        A = 15.0*q + np.power(0.9*q,3./2.) + np.power(5.6*q,2.)
        B = 1. + np.power(A,1.24)
        return np.power(5.6*q,2.) * np.power(B,-1./1.24)

    def function(self,k):
        if(self.perturbations == 'adiabatic'):
            transfer = self.adiabatic(k)
        elif(self.perturbations == 'isocurvature'):
            transfer = self.isocurvature(k)
        return transfer

class Bode01(Growth):

    def __init__(self, wdm_mass=0.5, g_x=1.5, nu=1.2, **kwargs):
        super(Bode01, self).__init__(**kwargs)
        self.m_x = wdm_mass
        self.g_x = g_x
        self.nu = nu
        self.omegaX = self.omegaM # Can only implement a single species... for now

    def alpha(self):
        return 0.048*np.power(self.omegaX/0.4,0.15) * np.power(self.h0/0.65,1./3.) * np.power(1./self.m_x,1.15) * np.power(1.5/self.g_x,0.29)

    def function(self, k):
        kh = k * self.h0 # 1/Mpc
        A = 1. + np.power(self.alpha()*k,2.*self.nu)
        return np.power(A,-5./self.nu)

class Eisenstein98_zb(Growth):

    def __init__(self, **kwargs):
        super(Eisenstein98_zb, self).__init__(**kwargs)
        self.omega0 = self.omegaM + self.omegab
        self.thetaCMB = self.Tcmb0/2.7

    # Equations (28-29)
    def function(self, k):
        kh = k * self.h0 #1/Mpc
        Gamma = self.omega0 * self.h0
        q = k/Gamma * self.thetaCMB**2.
        c0 = 14.2 + (731./(1. + 62.5*q))
        l0 = np.log(2.*np.e + 1.8*q)
        return l0/(l0 + c0*np.power(q,2))

class Eisenstein98(Growth):

    def __init__(self, **kwargs):
        super(Eisenstein98, self).__init__(**kwargs)
        #self.omega0 = self.omegaM
        self.omega0 = self.omegaM + self.omegab
        self.omega0hh = self.omega0 * np.power(self.h0,2.)
        self.omegabhh = self.omegab * np.power(self.h0,2.)
        self.baryfrac = self.omegab/self.omega0
        self.thetaCMB = self.Tcmb0/2.7

    # Equation (16)
    def function(self, k):
        if(self.omegab == 0.):
            print "This function is not well behaved when having a null baryonic matter density... use the zero baryon limit instead."
            sys.exit(1337)
        else:
            pass
        kh = k * self.h0 # 1/Mpc
        A = (self.baryfrac) * self.Tb(k)
        B = (1.-self.baryfrac) * self.Tcdm(k)
        return A + B

    # Equation (10)
    def q(self,k):
        return k/(13.41 * self.keq())

    # Equation (19-20)
    def tildeT0(self, k, alpha, beta):
        q = self.q(k)
        C = 14.2/alpha + 386./(1. + 69.9*np.power(q,1.08))
        A = np.e + (1.8 * beta * q)
        return np.log(A)/(np.log(A) + C*q*q)

    # Equation (17-18)
    def Tcdm(self, k):
        ks = k * self.s()
        f = 1./(1. + np.power(ks/5.4,4))
        A = f * self.tildeT0(k,1.,self.beta_cdm())
        B = (1. - f) * self.tildeT0(k,self.alpha_cdm(),self.beta_cdm())
        return A + B

    # Equation (11)
    def alpha_cdm(self):
        a1 = np.power(46.9*self.omega0hh,0.670) * (1. + np.power(32.1*self.omega0hh,-0.532))
        a2 = np.power(12.0*self.omega0hh,0.424) * (1. + np.power(45.0*self.omega0hh,-0.582))
        return np.power(a1,-self.baryfrac) * np.power(a2,-np.power(self.baryfrac,3.))

    # Equation (12)
    def beta_cdm(self):
        b1 = 0.944/(1. + np.power(458.*self.omega0hh,-0.708))
        b2 = np.power(0.395*self.omega0hh,-0.0266)
        return 1./(1. + b1 * (np.power(1.-self.baryfrac,b2) - 1.))

    # Equation (21)
    def Tb(self,k):
        ks = k * self.s()
        A = self.tildeT0(k,1.,1.)/(1. + np.power(ks/5.2,2.))
        B = np.exp(-np.power(k/self.ksilk(),1.4)) * (self.alpha_b()/(1. + np.power(self.beta_b()/ks,3.)))
        return self.j0(k*self.tildes(k)) * (A + B)

    # Equation (14)
    def alpha_b(self):
        zeq = self.zeq()
        keq = self.keq()
        zd = self.zd()
        Rd = self.R(zd)
        A = 2.07 * keq * self.s() * np.power(1.+Rd,-0.75)
        B = self.G((1. + zeq)/(1. + zd))
        return A * B

    # Equation (24)
    def beta_b(self):
        C = (3. - 2.*self.baryfrac) * np.sqrt(np.power(17.2*self.omega0hh,2) + 1.)
        return 0.5 + self.baryfrac + C

    # Equation (23)
    def beta_node(self):
        return 8.41 * np.power(self.omega0hh,0.435)

    # Equation (22)
    def tildes(self,k):
        D = 1. + np.power(self.beta_node()/k/self.s(),3.)
        return self.s()/np.power(D,1./3)

    # Zeroeth Spherical Bessel Function
    def j0(self,x):
        return np.sin(x)/x

    # Equation (15)
    def G(self,y):
        A = np.sqrt(1.+y) + 1.
        B = np.sqrt(1.+y) - 1.
        return y * (-6.*np.sqrt(1.+y) +(2.+3.*y)*np.log(A/B))

    # Equation (2)
    def zeq(self):
        return 2.5e+4 * self.omega0hh * np.power(self.thetaCMB,-4.)

    # Equation (3)
    def keq(self):
        return 7.46e-2 * self.omega0hh * np.power(self.thetaCMB,-2.) # 1/Mpc

    # Equation (4)
    def zd(self):
        b1 = 0.313 * np.power(self.omega0hh,-0.419) * (1. + 0.607*np.power(self.omega0hh,0.674))
        b2 = 0.238 * np.power(self.omega0hh,0.223)
        return 1291. * (np.power(self.omega0hh,0.251)/(1. + 0.659*np.power(self.omega0hh,0.828))) * (1.+b1*np.power(self.omegabhh,b2))

    # Equation (5)
    def R(self,z):
        return 31.5 * self.omegabhh * np.power(self.thetaCMB, -4.) * (1000./z)

    # Equation (6)
    def s(self):
        Req = self.R(self.zeq())
        Rd = self.R(self.zd())
        keq = self.keq()
        A = (2./(3.*keq)) * np.sqrt(6./Req)
        B = np.sqrt(1. + Rd) + np.sqrt(Rd+Req)
        C = 1. + np.sqrt(Req)
        return A * np.log(B/C)

    # Equation (7)
    def ksilk(self):
        return 1.6 * np.power(self.omegabhh,0.52) * np.power(self.omega0hh,0.73) * (1. + np.power(10.4*self.omega0hh,-0.95)) # 1/Mpc

