#!/usr/bin/env python 2

# system ----
import os
import sys
import numpy as np

# local ----
import cosmology


class Virial(cosmology.Cosmology):

    def __init__(self, **kwargs):
        super(Virial,self).__init__(**kwargs)

    def overdensity(self,z):
        xi = self.omega(z) - 1.
        return 18.*np.power(np.pi,2) + 82.*xi - 39.*np.power(xi,2.)

    def mass(self,z, R):
        convert = 1.
        return 4.*np.pi/3 * self.rho_c(z) * self.overdenisty(z) * np.power(R,3) # Msol

    def radius(self,z,M):
        convert = 1
        return np.power(3.*M/(4.*np.pi*self.rho_c(z)*self.overdensity(z)),1./3)
