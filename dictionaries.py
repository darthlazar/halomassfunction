#!/usr/bin/env python2

# system ----
from __future__ import absolute_import, division
import os
import sys

# /* ------------------- *\
# |* COSMOLOGICAL MODELS *|
# \* ------------------- */

cosmology_dict = {}
cosmology_dict['PLANCK18'] = {'h0' : 0.6766, 'omegaM' : 0.3111, 'omegab' : 0.0490, 'n_s' : 0.9665, 'sigma8' : 0.8102, 'rel' : True}
cosmology_dict['PLANCK15'] = {'h0' : 0.6774, 'omegaM' : 0.3089, 'omegab' : 0.0486, 'n_s' : 0.9667, 'sigma8' : 0.8159, 'rel' : True}
cosmology_dict['PLANCK13'] = {'h0' : 0.6777, 'omegaM' : 0.3071, 'omegab' : 0.0483, 'n_s' : 0.9611, 'sigma8' : 0.8288, 'rel' : True}
cosmology_dict['WMAP9']    = {'h0' : 0.6932, 'omegaM' : 0.2865, 'omegab' : 0.0463, 'n_s' : 0.9608, 'sigma8' : 0.8200, 'rel' : True}
cosmology_dict['WMAP7']    = {'h0' : 0.7020, 'omegaM' : 0.2743, 'omegab' : 0.0458, 'n_s' : 0.9680, 'sigma8' : 0.8160, 'rel' : True}
cosmology_dict['WMAP5']    = {'h0' : 0.7050, 'omegaM' : 0.2732, 'omegab' : 0.0456, 'n_s' : 0.9600, 'sigma8' : 0.8120, 'rel' : True}
cosmology_dict['WMAP3']    = {'h0' : 0.7350, 'omegaM' : 0.2342, 'omegab' : 0.0413, 'n_s' : 0.9510, 'sigma8' : 0.7420, 'rel' : True}
cosmology_dict['WMAP1']    = {'h0' : 0.7200, 'omegaM' : 0.2700, 'omegab' : 0.0463, 'n_s' : 0.9900, 'sigma8' : 0.9000, 'rel' : True}
cosmology_dict['EdS']      = {'h0' : 0.7000, 'omegaM' : 1.0000, 'omegab' : 0.0000, 'n_s' : 1.0000, 'sigma8' : 0.8200, 'rel' : False}

# /* ------------------------ *\
# |* TRANSFER FUNCTION MODELS *|
# \* ------------------------ */

transfer_dict = {
        'BARDEEN86'       : 'bardeen86',
        'EISENSTEIN98'    : 'eisenstein98',
        'EISENSTEIN98_ZB' : 'eisenstein98_zb'
        }

# /* ---------------------- *\
# |* FILTER FUNCTION MODELS *|
# \* ---------------------- */

filter_dict = {
        'TOPHAT'   : 'tophat',
        'GAUSSIAN' : 'gaussian',
        'K_SPACE'  : 'sharp_k'
        }

# /* --------------------- *\
# |* HMF FITTING FUNCTIONS *|
# \* --------------------- */

halo_dict = {
        'vir'  : 'vir',
        '200c' : '200c',
        '200m' : '200m',
        '500c' : '500c',
        '500m' : '500m'
        }

# /* --------------------- *\
# |* HMF FITTING FUNCTIONS *|
# \* --------------------- */

fit_dict = {
        'PRESS74'        : 'press74',
        'SHETH99'        : 'sheth99',
        'JENKINS01'      : 'jenkins01',
        'REED03'         : 'reed03',
        'WARREN06'       : 'warren06',
        'REED07'         : 'reed07',
        'TINKER08'       : 'tinker08',
        'CROCCE10'       : 'crocce10',
        'COURTIN'        : 'courtin10',
        'BHATTACHARYA11' : 'bharracharya11',
        'ANGULO12'       : 'angulo12',
        'ANGULO12_SUB'   : 'angulo12_sub',
        'WATSON13_FOF'   : 'watson13_fof',
        'WATSON13_SO'    : 'watson13_so'
        }

# \* --------------------- */

all_dict = {
        'cosmology_dict': cosmology_dict,
        'transfer_dict' : transfer_dict,
        'filter_dict'   : filter_dict,
        'fit_dict'      : fit_dict
        }

