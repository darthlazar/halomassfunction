#!/usr/bin/env python2

# system ----
from __future__ import absolute_import, division
import os
import sys
import numpy as np
import scipy.integrate as integrate
import warnings
warnings.filterwarnings("ignore")

# local ----
import transfer
import filter
from cosmology import Cosmology

class Linear(transfer.Transfer):

    def __init__(self, transfer_model='eisenstein98', filter_shape='tophat', wdm_effects=False, **kwargs):
        super(Linear, self).__init__(**kwargs)
        self.transfer_model = transfer_model
        self.wdm_effects = wdm_effects
        self.transfer_init = transfer.Transfer(**kwargs)
        self.filter_shape = filter.FilterShape(**kwargs).__getattribute__(filter_shape)

    def transfer_func(self, k):
        transfer_func = lambda x: self.transfer_init.__getattribute__(self.transfer_model).function(x)
        cdm_transfer = transfer_func(k)
        if(self.wdm_effects == True):
            wdm_transfer = lambda y: self.transfer_init.__getattribute__('bode01').function(y)
            cdm_transfer *= wdm_transfer(k)
        else:
            pass
        return cdm_transfer

    def primordial(self,k):
        return np.power(k,self.n_s)

    def unscaled_spectrum(self, k):
        return np.power(self.transfer_func(k),2) * self.primordial(k)

    def unscaled_sigma(self, M):
        if(type(M) is np.ndarray):
            results = np.zeros(M.size, dtype='float')
            for i in range(0, M.size):
                integrand = lambda x: self.sigma_func(x, M[i])
                results[i] = np.sqrt(integrate.quad(integrand, 0.0, np.inf, limit=100)[0])
        else:
            integrand = lambda x: self.sigma_func(x, M)
            results = np.sqrt(integrate.quad(integrand, 0.0, np.inf, limit=100)[0])
        return results

    def spectrum(self, z, k):
        return self.sigma8norm() * np.power(self.d(z),2) * np.power(self.transfer_func(k),2) * self.primordial(k)

    def dimensionless(self, z, k):
        return np.power(k,3)/(2.*np.power(np.pi,2)) * self.spectrum(z,k)

    def sigma(self, z, M):
        return np.sqrt(self.sigma8norm()) * self.d(z) * self.unscaled_sigma(M)

    def sigma_func(self, k, M):
        return np.power(k,2) * self.unscaled_spectrum(k) * np.power(abs(self.window(k,M)),2) * 1./(2.*np.power(np.pi,2))

    def window(self, k, M):
        R = self.filter_shape.radius(M)
        return self.filter_shape.filter_func(k,R)

    def sigma8norm(self):
        M8 = self.filter_shape.mass(8.)
        sig8 = self.unscaled_sigma(M8)
        return np.power(self.sigma8,2)/np.power(sig8,2)





