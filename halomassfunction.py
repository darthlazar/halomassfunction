#!/usr/bin/env python2

# system ----
import os
import sys
import numpy as np
from scipy import interpolate

# local ----
import power
import fittings

class Function(fittings.Functions):

    def __init__(self, fit='press74', **kwargs):
        super(Function, self).__init__(**kwargs)
        self.fit_function = fittings.Functions(**kwargs).__getattribute__(fit)

    def dM(self, z, M):
        sigma = self.sigma(z,M)
        tck = interpolate.splrep(np.log(M), np.log(sigma))
        ds = - interpolate.splev(np.log(M), tck, der=1)
        rho_convert = self.rho_m(0.0) * 5.e-34/np.power(3.24e-25,3.) # g/cm^3 --> Msol/Mpc^3
        ds *= rho_convert/np.power(M,2)
        return self.fit_function.fsigma(z, M) * ds

    def dlnM(self, z, M):
        sigma = self.sigma(z,M)
        tck = interpolate.splrep(np.log(M), np.log(sigma))
        ds = - interpolate.splev(np.log(M), tck, der=1)
        rho_convert = self.rho_m(0.0) * 5.e-34/np.power(3.24e-25,3.) # g/cm^3 --> Msol/Mpc^3
        ds *= rho_convert/M
        return self.fit_function.fsigma(z, M) * ds

    def dlogM(self, z, M):
        sigma = self.sigma(z,M)
        tck = interpolate.splrep(np.log10(M), np.log10(sigma))
        ds = - interpolate.splev(np.log10(M), tck, der=1)
        rho_convert = self.rho_m(0.0) * 5.e-34/np.power(3.24e-25,3.) # g/cm^3 --> Msol/Mpc^3
        ds *= rho_convert/M
        return self.fit_function.fsigma(z, M) * ds

