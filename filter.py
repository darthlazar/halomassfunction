#!/usr/bin/env python2

# system ----
from __future__ import absolute_import, division
import os
import sys
import warnings
import numpy as np

# local ----
import cosmology

class FilterShape(cosmology.Cosmology):

    def __init__(self, **kwargs):
        super(FilterShape, self).__init__(**kwargs)
        self.tophat = Tophat(**kwargs)
        self.gaussian = Gaussian(**kwargs)
        self.sharpk = SharpK(**kwargs)

class Tophat(cosmology.Cosmology):

    def __init__(self, **kwargs):
        super(Tophat, self).__init__(**kwargs)

    def mass(self,R):
        rho_convert = self.rho_m(0.0) * 5.e-34/np.power(3.24e-25,3.) # g/cm^3 --> Msol/Mpc^3
        return (4.*np.pi/3.) * rho_convert * np.power(R,3) # Msol

    def radius(self,M):
        rho_convert = self.rho_m(0.0) * 5.e-34/np.power(3.24e-25,3.) # g/cm^3 --> Msol/Mpc^3
        return np.power(3.*M/(4.*np.pi*rho_convert), 1/3.) # Mpc

    def filter_func(self,k,R):
        kR = k*R
        result = 3. * (np.sin(kR) - kR*np.cos(kR))/np.power(kR,3)
        return result

class Gaussian(cosmology.Cosmology):

    def __init__(self, **kwargs):
        super(Gaussian, self).__init__(**kwargs)

    def mass(self,R):
        rho_convert = self.rho_m(0.0) * 5e-34/np.power(3.24e-25,3.) # g/cm^3 --> Msol/Mpc^3
        return np.power(2.*np.pi,1.5) * np.pi * rho_convert * np.power(R,3.)

    def radius(self, M):
        rho_convert = self.rho_m(0.0) * 5e-34/np.power(3.24e-25,3.) # g/cm^3 --> Msol/Mpc^3
        return np.power(M/(np.power(2.*np.pi,1.5)*np.pi*rho_convert),1./3)

    def filter_func(self,k,R):
        kR = k*R
        return np.exp(-np.power(kR)/2.)

class SharpK(cosmology.Cosmology):

    def __init__(self, **kwargs):
        super(SharpK, self).__init__(**kwargs)
        self.c = np.power(9.*np.pi/2,1./3)

    def mass(self,R):
        return Tophat.mass(R)/np.power(self.c,3)

    def radius(self,M):
        return Tophat.radius(M)/self.c

    def filter_func(self,k,R):
        kR = k*R
        return np.heaviside(kR, 1.)


