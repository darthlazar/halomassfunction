#!/usr/bin/env python2

# system ----
import os
import sys
import numpy as np
from scipy import interpolate

# local ----
import power
import halomodel

class Functions(power.Linear):

    def __init__(self, **kwargs):
        super(Functions,self).__init__(**kwargs)
        self.press74      = Press74(**kwargs)
        self.sheth99      = Sheth99(**kwargs)
        self.jenkins01    = Jenkins01(**kwargs)
        self.reed03       = Reed03(**kwargs)
        self.warren06     = Warren06(**kwargs)
        self.reed07       = Reed07(**kwargs)
        self.tinker08     = Tinker08(**kwargs)
        self.crocce10     = Crocce10(**kwargs)
        self.courtin10    = Courtin10(**kwargs)
        self.bhatta11     = Bhattacharya11(**kwargs)
        self.angulo12     = Angulo12(**kwargs)
        self.angulo12_sub = Angulo12_SUB(**kwargs)
        self.watson13_fof = Watson13_FOF(**kwargs)
        self.watson13_so  = Watson13_SO(**kwargs)

class Press74(power.Linear):

    def __init__(self, **kwargs):
        super(Press74, self).__init__(**kwargs)
        self.delta_c = 1.686

    def fsigma(self, z, M):
        sigma = self.sigma(z, M)
        nu = self.delta_c/sigma
        return np.sqrt(2./np.pi) * nu * np.exp(-np.power(nu,2)/2.)

class Sheth99(power.Linear):

    def __init__(self, **kwargs):
        super(Sheth99, self).__init__(**kwargs)
        self.delta_c = 1.686

    def fsigma(self, z, M):
        sigma = self.sigma(z, M)
        nu = self.delta_c/sigma
        A, a, p = [0.3222, 0.707, 0.3]
        return A * np.sqrt(2.*a/np.pi) * (1. + np.power(np.power(nu,2)/a,p)) * nu * np.exp(-a*np.power(nu,2)/2.)

class Jenkins01(power.Linear):

    def __init__(self, **kwargs):
        super(Jenkins01, self).__init__(**kwargs)

    def fsigma(self, z, M):
        sigma = self.sigma(z, M)
        A, a, b = [0.315, 0.61, 3.8]
        ln_inv_sigma = np.log(1./sigma)
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(-1.2 < ln_inv_sigma[i] < 1.05):
                value = abs(ln_inv_sigma[i] + a)
                results[i] = A * np.exp(-np.power(value,b))
            else:
                results[i] = 0.
        return results

class Reed03(Sheth99):

    def __init__(self, **kwargs):
        super(Reed03, self).__init__(**kwargs)
        self.sheth = Sheth99(**kwargs)

    def fsigma(self, z, M):
        sigma = self.sigma(z, M)
        ln_inv_sigma = np.log(1./sigma)
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(-1.7 < ln_inv_sigma[i] < 0.9):
                value = sigma[i] * np.power(np.cosh(2.*sigma[i]), 5)
                results[i] = self.sheth.fsigma(z, M[i]) * np.exp(-0.7/value)
            else:
                results[i] = 0.
        return results

class Warren06(power.Linear):

    def __init__(self, **kwargs):
        super(Warren06, self).__init__(**kwargs)

    def fsigma(self, z, M):
        sigma = self.sigma(z, M)
        A, b, C, d = [0.7234, -1.625, 0.2538, -1.1982]
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(1e+10 < M[i] < 1e+15):
                results[i] =  A * (np.power(sigma[i],b) + C) * np.exp(d/np.power(sigma[i],2))
            else:
                results[i] = 0
        return results

class Reed07(power.Linear):

    def __init__(self, **kwargs):
        super(Reed07, self).__init__(**kwargs)
        self.delta_c = 1.686

    def G1(self, sigma):
        return np.exp(-np.power(np.log(1./sigma - 0.4),2)/0.72)

    def G2(self, sigma):
        return np.exp(-np.power(np.log(1./sigma - 0.75),2)/0.08)

    def neff(self, z, M): # computer derivative via spline interpolation or use the approximation
        sigma = self.sigma(z, M)
        ln_inv_sigma = np.log(1./sigma)
        mz = 0.55 - 0.32*np.power((1.-1./(1.+z)),5)
        rz = -1.74 - 0.8*np.power(abs(np.log(1./(1.+z))),0.8)
        return mz*ln_inv_sigma + rz

    def fsigma(self, z, M):
        A, a, c, p = [0.3222, 0.707, 1.08, 0.3]
        sigma = self.sigma(z, M)
        ln_inv_sigma = np.log(1./sigma)
        nu = self.delta_c/sigma
        neff = self.neff(z, M)
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(-1.7 < ln_inv_sigma[i] < 0.9):
                alpha = -c*a*np.power(nu[i],2)/2. - 0.03*np.power(nu[i],0.6)/np.power(neff[i] + 3.,2)
                beta = 1. + np.power(1./(np.power(nu[i],2)*a),p) + 0.6*self.G1(sigma[i]) + 0.4*self.G2(sigma[i])
                results[i] = nu[i] * np.exp(alpha) * A * np.sqrt(2.*a/np.pi) * beta
            else:
                results[i] = 0
        return results

class Tinker08(power.Linear):

    def __init__(self, **kwargs):
        super(Tinker08, self).__init__(**kwargs)
        self.delta_c = 1.686
        self.virial = halomodel.Virial(**kwargs)

    def A(self, z):
        return 0.186 * np.power(1.+z,-0.14)

    def a(self, z):
        return 1.47 * np.power(1.+z,-0.06)

    def b(self, z):
        return 2.57 * np.power(1.+z,-self.alpha(z))

    def alpha(self, z):
        vir = self.virial.overdensity(z)
        return np.exp(-np.power(0.75/np.log(vir/75.),1.2))

    def fsigma(self, z, M):
        A = self.A(z)
        a = self.a(z)
        b = self.b(z)
        c = 1.19
        sigma = self.sigma(z, M)
        ln_inv_sigma = np.log(1./sigma)
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(-0.6 < ln_inv_sigma[i] < 0.4):
                results[i] = A * (np.power(b/sigma[i],a) + 1.) * np.exp(-c/np.power(sigma[i],2.))
            else:
                results[i] = 0
        return results

class Crocce10(power.Linear):

    def __init__(self, **kwargs):
        super(Crocce10, self).__init__(**kwargs)

    def A(self,z):
        return 0.58 * np.power(1.+z,-0.13)

    def a(self,z):
        return 1.37 * np.power(1.+z,-0.15)

    def b(self,z):
        return 0.3 * np.power(1.+z,-0.084)

    def c(self,z):
        return 1.036 * np.power(1.+z,-0.024)

    def fsigma(self, z, M):
        A = self.A(z)
        a = self.a(z)
        b = self.b(z)
        c = self.c(z)
        sigma = self.sigma(z, M)
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(np.power(10.,10.5) < M[i] < np.power(10., 15.5)):
                results[i] = A * (np.power(sigma[i],-a) + b) * np.exp(-c/np.power(sigma[i],2))
            else:
                results[i] = 0
        return results

class Courtin10(power.Linear):

    def __init__(self, **kwargs):
        super(Courtin10, self).__init__(**kwargs)
        self.delta_c = 1.686

    def fsigma(self, z, M):
        A, a, p = [0.348, 0.695, 0.1]
        sigma = self.sigma(z, M)
        ln_inv_sigma = np.log(1./sigma)
        nu = self.delta_c/sigma
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(-0.8 < ln_inv_sigma[i] < 0.7):
                results[i] = A * np.sqrt(2.*a/np.pi) * (1. + np.power(np.power(nu[i],2)/a,p)) * nu[i] * np.exp(-a*np.power(nu[i],2)/2.)
            else:
                results[i] = 0
        return results

class Bhattacharya11(power.Linear):

    def __init__(self, **kwargs):
        super(Bhattacharya11, self).__init__(**kwargs)
        self.delta_c = 1.686

    def A(self,z):
        return 0.333/np.power(1.+z,0.11)

    def a(self, z):
        return 0.788/np.power(1.+z,0.01)

    def fsigma(self, z, M):
        A = self.A(z)
        a = self.a(z)
        p = 0.807
        q = 1.795
        sigma = self.sigma(z, M)
        nu = self.delta_c/sigma
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(np.power(10.,11.8) < M[i] < np.power(10., 15.5)):
                alpha = a * np.power(nu[i],2)
                beta = nu[i] * np.sqrt(a)
                results[i] = A * np.sqrt(2./np.pi) * np.exp(-alpha/2.) * (1. + np.power(alpha,-p)) * np.power(beta,q)
            else:
                results[i] = 0
        return results

class Angulo12(power.Linear):

    def __init__(self, **kwargs):
        super(Angulo12, self).__init__(**kwargs)

    def fsigma(self, z, M):
        A, a, b, c = [0.201, 1.7, 2.08, 1.172]
        sigma = self.sigma(z, M)
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(np.power(10.,8) < M[i] < np.power(10., 16)):
                results[i] = A * (np.power(b/sigma[i],a) + 1.) * np.exp(-c/np.power(sigma[i],2))
            else:
                results[i] = 0
        return results

class Angulo12_SUB(power.Linear):

    def __init__(self, **kwargs):
        super(Angulo12_SUB, self).__init__(**kwargs)

    def fsigma(self, z, M):
        A, a, b, c = [0.265, 1.9, 1.675, 1.4]
        sigma = self.sigma(z, M)
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(np.power(10.,8) < M[i] < np.power(10., 16)):
                results[i] = A * (np.power(b/sigma[i],a) + 1.) * np.exp(-c/np.power(sigma[i],2))
            else:
                results[i] = 0
        return results

class Watson13_FOF(power.Linear):

    def __init__(self, **kwargs):
        super(Watson13_FOF, self).__init__(**kwargs)

    def fsigma(self, z, M):
        A, a, b, c = [0.282, 1.406, 2.163, 1.21]
        sigma = self.sigma(z, M)
        ln_inv_sigma = np.log(1./sigma)
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(-0.55 < ln_inv_sigma[i] < 1.31):
                results[i] = A * (np.power(b/sigma[i],a) + 1.) * np.exp(-c/np.power(sigma[i],2.))
            else:
                results[i] = 0
        return results

# This needs to be fixed
class Watson13_SO(power.Linear):

    def __init__(self, **kwargs):
        super(Watson13_SO, self).__init__(**kwargs)
        self.delta_c = 1.686
        self.virial = halomodel.Virial(**kwargs)

    def constants(self,z):
        if(z == 0.0):
            A,alpha,beta,gamma = [0.194, 1.805, 2.267, 1.287]
        elif(0.0 < z < 6.0):
            A = self.omega(z) * (1.097 * np.power(1.+z,-3.216) + 0.074)
            alpha = self.omega(z) * (5.907 * np.power(1.+z,-3.599) + 2.344)
            beta = self.omega(z) * (3.136 * np.power(1.+z,-3.058) + 2.349)
            gamma = 1.318
        elif(z > 6.):
            A,alpha,beta,gamma = [0.563, 3.180, 0.874, 1.453]
        return [A,alpha,beta,gamma]

    def Gamma(self, sigma, z):
        q = 2.130
        p = 0.072
        #Delta = self.virial.overdensity(z)
        Delta = 200
        return self.C(z) * np.power(Delta/178.,self.d(z)) * np.exp(p*(1.-Delta/178.)/np.power(sigma,q))

    def C(self, z):
        Delta = 200
        #Delta = self.virial.overdensity(z)
        return np.exp(0.023*(Delta/178.-1.))

    def d(self,z):
        return -0.456*self.omega(z) - 0.139

    def sigma_limits(self,z):
        if(z == 0.0):
            _min = -0.55
            _max = 1.05
        elif(z > 0.0):
            _min = -0.06
            _max = 1.024
        return [_min, _max]

    def fsigma(self, z, M):
        A, alpha, beta, gamma = self.constants(z)
        sigma = self.sigma(z, M)
        ln_inv_sigma = np.log(1./sigma)
        _min, _max = self.sigma_limits(z)
        results = np.zeros(M.size, dtype='float')
        for i in range(0, M.size):
            if(_min < ln_inv_sigma[i] < _max):
                results[i] = self.Gamma(sigma[i],z) * A * (np.power(beta/sigma[i],alpha) + 1.)
            else:
                results[i] = 0
        return results

